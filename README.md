# BoviSync Code Styleguide

A centralized location to learn and download the Bovisync code styles.

## Functions
funtion styling
### Parameters
The beginning list of parameters in functions should be preceeded with a space and after the last parameter should be followed with another space. Each parameter should be seperated by a comma and a space.

``` javascript
// Good
this.getData = ( dataType, otherParam ) => {
  console.log('Data got');
}

function( param1, param2 ){
  return param1 + param2;
}

// Bad
this.getData = (dataType,otherParam) => {
  console.log('Data got');
}

function(param1,param2){
  return param1 + param2;
}
```
## Strings
### Use single-quotes
For most strings use single quotes.

``` javascript
// GOOD
const name = 'Chad';

// BAD - template literals should contain interpolations or newlines
const name = `Chad`

//BAD
const name = "Chad";
```
## If-Else Statements
If-Else statments should be formatted similarly to functions. If-Else and Else statements should be on a new line after the closing bracket.

``` javascript
// GOOD
if( isTrue ) {
  isTrue = false
}
else if( isFalse ) {
  isFalse = false
}
else {
  isTrue = true;
  isFalse = true;
}


// BAD
if(isTrue) {
  isTrue = false
} else {
  isTrue = true;
}

if(isTrue){ isTrue = false }
else { isTrue = true }
```
## Whitespace
### Use Soft Tabs
Use soft tabs set to 2 spaces. No tabs allowed.
``` javascript
// bad
function foo() {
∙∙∙∙let name;
}

// bad
function Sna(){
→→→→let name;
}

// bad
function bar() {
∙let name;
}

// good
function baz() {
∙∙let name;
}
```
## Line Endings

Line Endings should be commited as the unix style LF, rather than CRLF. If you're on a Windows machine, using CRLF, ensure your git is setup to convert all line endings to LF. 

```
// ON WINDOWS
[core]
    autocrlf = true

// ON UNIX
[core]
    autocrlf = input
```